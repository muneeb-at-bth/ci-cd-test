const Parser = require('expr-eval').Parser

class Calculator {
  add (...numbers) {
    return numbers.reduce((s, x) => s + x, 0)
  }

  subtract (...numbers) {
    return numbers.reduce((s, x) => s - x)
  }

  multiply (...numbers) {
    return numbers.reduce((s, x) => s * x, 1)
  }

  divide (dividend, divisor) {
    return dividend / divisor
  }

  evalExpression (expression) {
    return Parser.evaluate(decodeURI(expression))
  }
}

module.exports = new Calculator()
