/* eslint-disable no-undef */
const calc = require('../util/calculator')

describe('Calculator Tests', () => {
  test('add: 1 + 2 to equal 3', () => {
    expect(calc.add(1, 2)).toBe(3)
  })

  test('subtract: 10 - 3 to equal 7', () => {
    expect(calc.subtract(10, 3)).toBe(7)
  })

  test('divide: 8 / 2 to equal 4', () => {
    expect(calc.divide(8, 4)).toBe(2)
  })

  test('multiply: 2 * 3 to equal 6', () => {
    expect(calc.multiply(2, 3)).toBe(6)
  })

  test('evaluate: (2 * 3 + 1) to equal 7', () => {
    expect(calc.evalExpression('2 * 3 + 1')).toBe(7)
  })
   test('multiply: 4 * 2 to equal 8', () => {
    expect(calc.multiply(4, 2)).toBe(8)
  })

  test('multiply: 3 * 3 to equal 9', () => {
    expect(calc.multiply(3, 3)).toBe(9)
  })

  test('add: 5 + 3 to equal 8', () => {
    expect(calc.add(5, 3)).toBe(8)
  })
})
