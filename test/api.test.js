/* eslint-disable no-undef */
const request = require('supertest')
const app = require('../app')

describe('API Test', () => {
  it('GET:/ responds with 200', function (done) {
    request(app)
      .get('/')
      .set('Accept', 'application/json; charset=utf-8')
      .expect('Content-Type', 'application/json; charset=utf-8')
      .expect(200, done)
  })

  it('GET:/add responds with 200', function (done) {
    request(app)
      .get('/add/1,2')
      .set('Accept', 'application/json; charset=utf-8')
      .expect('Content-Type', 'application/json; charset=utf-8')
      .expect(200)
      .then(response => {
        if (response.body.data !== 3) throw new Error('invalid response')
        done()
      })
      .catch(err => done(err))
  })

  it('GET:/sub responds with 200', function (done) {
    request(app)
      .get('/sub/10,3')
      .set('Accept', 'application/json; charset=utf-8')
      .expect('Content-Type', 'application/json; charset=utf-8')
      .expect(200)
      .then(response => {
        if (response.body.data !== 7) throw new Error('invalid response')
        done()
      })
      .catch(err => done(err))
  })

  it('GET:/mul responds with 200', function (done) {
    request(app)
      .get('/mul/2,3')
      .set('Accept', 'application/json; charset=utf-8')
      .expect('Content-Type', 'application/json; charset=utf-8')
      .expect(200)
      .then(response => {
        if (response.body.data !== 6) throw new Error('invalid response')
        done()
      })
      .catch(err => done(err))
  })

  it('GET:/divide responds with 200', function (done) {
    request(app)
      .get('/divide?dividend=8&divisor=4')
      .set('Accept', 'application/json; charset=utf-8')
      .expect('Content-Type', 'application/json; charset=utf-8')
      .expect(200)
      .then(response => {
        if (response.body.data !== 2) throw new Error('invalid response')
        done()
      })
      .catch(err => done(err))
  })

  it('GET:/eval responds with 200', function (done) {
    request(app)
      .get('/eval?expression=' + encodeURIComponent('2 * 3 + 1'))
      .set('Accept', 'application/json; charset=utf-8')
      .expect('Content-Type', 'application/json; charset=utf-8')
      .expect(200)
      .then(response => {
        if (response.body.data !== 7) throw new Error('invalid response')
        done()
      })
      .catch(err => done(err))
  })
})
