const calc = require('../util/calculator')

class IndexController {
  index (req, res) {
    return res.status(200).json({ msg: 'server is running' })
  }

  add (req, res) {
    if (!req.params || !req.params.numbers.split()) return res.status(400).json({ msg: 'invalid param' })
    const numbers = req.params.numbers.split(',')
    return res.status(200).json({ data: calc.add(...numbers.map(x => Number(x))) })
  }

  subtract (req, res) {
    if (!req.params || !req.params.numbers.split()) return res.status(400).json({ msg: 'invalid param' })
    const numbers = req.params.numbers.split(',')
    return res.status(200).json({ data: calc.subtract(...numbers.map(x => Number(x))) })
  }

  multiply (req, res) {
    if (!req.params || !req.params.numbers.split()) return res.status(400).json({ msg: 'invalid param' })
    const numbers = req.params.numbers.split(',')
    return res.status(200).json({ data: calc.multiply(...numbers.map(x => Number(x))) })
  }

  divide (req, res) {
    if (!req.query.dividend || !req.query.divisor) return res.status(400).json({ msg: 'invalid param' })
    const dividend = Number(req.query.dividend)
    const divisor = Number(req.query.divisor)
    return res.status(200).json({ data: calc.divide(dividend, divisor) })
  }

  evalExpression (req, res) {
    if (!req.query.expression) return res.status(400).json({ msg: 'invalid param' })
    return res.status(200).json({ data: calc.evalExpression(req.query.expression) })
  }
}

module.exports = new IndexController()
