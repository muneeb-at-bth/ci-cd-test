# CI Demo Project


# Installation
First, you need Node.js runtime to execute these scripts.
[Get Node.js runtime here](https://nodejs.org/en/)
After installing runtime environment

    cd ./project
    npm i
    
    

## Startup Commands

For starting the server `npm run start`

For executing unit tests cases `npm run test-unit`

For executing unit tests cases `npm run test-api`

For checking linting errors `npm run lint`

For fixing lint errors `npm run lint-fix`
