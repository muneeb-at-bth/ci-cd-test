const express = require('express')

const router = express.Router()
const indexController = require('../controllers/indexController')

router.get('/', indexController.index)
router.get('/add/:numbers', indexController.add)
router.get('/sub/:numbers', indexController.subtract)
router.get('/mul/:numbers', indexController.multiply)
router.get('/divide', indexController.divide)
router.get('/eval', indexController.evalExpression)

module.exports = router
